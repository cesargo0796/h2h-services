package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaValoresPosibleAtributoResponse {
	private String valDespliegue; 
	private String valorAtributo;
}