package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoDebitar {
	public String valFamiliaProducto;
	public String valNumeroProducto;
	public String valSubtipoProducto;
	public String valTipoProducto;
}
