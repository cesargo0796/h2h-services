package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultarServiciosPublicosRequest{     
	@JsonProperty("request")
   private RequestConsultarServiciosPublicos request; 
}