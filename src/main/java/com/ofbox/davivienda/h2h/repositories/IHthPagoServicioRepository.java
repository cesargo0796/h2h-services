package com.ofbox.davivienda.h2h.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ofbox.davivienda.h2h.dto.HthPagoServicioDto;
import com.ofbox.davivienda.h2h.entities.HthPagoServicio;

@Repository
public interface IHthPagoServicioRepository extends JpaRepository<HthPagoServicio,Long> {
	
	@Query("SELECT new com.ofbox.davivienda.h2h.dto.HthPagoServicioDto"
			+ "(h.idPagoServicio, h.estado, h.numCuenta,h.bsCliente.cliente) "
			+ "FROM HthPagoServicio h WHERE h.numCuenta = :numeroCuenta AND h.bsCliente.cliente = :idCliente" )
	List<HthPagoServicioDto> findByNumCuenta(@Param("numeroCuenta") String numeroCuenta, @Param("idCliente") Long idCliente);
}
