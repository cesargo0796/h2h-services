package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestPagoServicios {
	@JsonProperty("data")
	private DataPagoServiciosRequest data;
	@JsonProperty("dataHeader")
	private DataHeaderPagoServiciosRequest dataHeader;

}
