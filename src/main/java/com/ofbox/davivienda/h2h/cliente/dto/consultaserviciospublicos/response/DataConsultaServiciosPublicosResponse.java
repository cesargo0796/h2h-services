package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataConsultaServiciosPublicosResponse {
	@JsonProperty("recibos")
	private List<RecibosConsultaServiciosPublicosResponse> recibos;
}
