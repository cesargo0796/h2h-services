package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataConsultarServiciosPublicos {
    public String codIdioma;
    public String codPais;
    public String codTipoIdentificacion;
    public String idSesion;
    public String valOrigen;
    public String valNumeroIdentificacion;
	@JsonProperty("listServicios")
    public List<ListServicio> listServicios;

}
