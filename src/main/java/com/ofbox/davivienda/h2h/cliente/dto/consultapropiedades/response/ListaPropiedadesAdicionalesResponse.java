package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaPropiedadesAdicionalesResponse {
	
   private String idPropiedad;
   private String valDescripcion;
   private String valOrden;
}