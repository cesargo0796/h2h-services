package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHeaderPagoServiciosPublicosResponse {

	private String caracterAceptacion;
	private Integer codMsgRespuesta;
	private String idTransaccion;
	private String msgRespuesta;
	private String nombreOperacion;
	private Integer total;
	private String ultimoMensaje;

}
