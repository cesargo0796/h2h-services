package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Dato {
    public String codDato;
    public String valDescripcionDato;

}
