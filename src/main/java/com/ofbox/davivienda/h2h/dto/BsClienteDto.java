package com.ofbox.davivienda.h2h.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BsClienteDto {
	
	private Long cliente;
	private String apellidoContacto;
	private String apellidoRepresentante;
	private Long banca;
	private Boolean bloqueado;
	private Boolean cargaPersonalizable;
	private String cargo;
	private String ciudad;
	private Long codigoTablaTransExt;
	private Double comisionTransExt;
	private Double comisionTrnACH;
	private String direccion;
	private String doctoRepresentante;
	private String email;
	private Long estatus;
	private String fax;
	private Date fechaCreacion;
	private Date fechaEstatus;
	private Double limiteCreditos;
	private Double limiteDebitos;
	private Double limiteXArchivo;
	private Double limiteXLote;
	private Double limiteXTransaccion;
	private Long modulos;
	private Long nivelSeguridadAFP;
	private Long nivelSeguridadCCre;
	private Long nivelSeguridadISSS;
	private Long nivelSeguridadPImp;
	private Long nivelSeguridadPPag;
	private Long nivelSeguridadTEx;
	private String nombre;
	private String nombreContacto;
	private String nombreRepresentante;
	private Long numInstalacion;
	private Boolean permiteDebitos;
	private Boolean permiteSMSToken;
	private Long politicaCobroTEx;
	private String proveedorInternet;
	private Long solicitudPendiente;
	private String telefono;
	private String telefonoSoporte;
	private Long tipoDoctoRepresentante;
	public BsClienteDto(Long cliente, String apellidoContacto, String apellidoRepresentante, Long banca,
			Boolean bloqueado, Boolean cargaPersonalizable, String cargo, String ciudad, Long codigoTablaTransExt,
			Double comisionTransExt, Double comisionTrnACH, String direccion, String doctoRepresentante, String email,
			Long estatus, String fax, Date fechaCreacion, Date fechaEstatus, Double limiteCreditos,
			Double limiteDebitos, Double limiteXArchivo, Double limiteXLote, Double limiteXTransaccion, Long modulos,
			Long nivelSeguridadAFP, Long nivelSeguridadCCre, Long nivelSeguridadISSS, Long nivelSeguridadPImp,
			Long nivelSeguridadPPag, Long nivelSeguridadTEx, String nombre, String nombreContacto,
			String nombreRepresentante, Long numInstalacion, Boolean permiteDebitos, Boolean permiteSMSToken,
			Long politicaCobroTEx, String proveedorInternet, Long solicitudPendiente, String telefono,
			String telefonoSoporte, Long tipoDoctoRepresentante) {
		this.cliente = cliente;
		this.apellidoContacto = apellidoContacto;
		this.apellidoRepresentante = apellidoRepresentante;
		this.banca = banca;
		this.bloqueado = bloqueado;
		this.cargaPersonalizable = cargaPersonalizable;
		this.cargo = cargo;
		this.ciudad = ciudad;
		this.codigoTablaTransExt = codigoTablaTransExt;
		this.comisionTransExt = comisionTransExt;
		this.comisionTrnACH = comisionTrnACH;
		this.direccion = direccion;
		this.doctoRepresentante = doctoRepresentante;
		this.email = email;
		this.estatus = estatus;
		this.fax = fax;
		this.fechaCreacion = fechaCreacion;
		this.fechaEstatus = fechaEstatus;
		this.limiteCreditos = limiteCreditos;
		this.limiteDebitos = limiteDebitos;
		this.limiteXArchivo = limiteXArchivo;
		this.limiteXLote = limiteXLote;
		this.limiteXTransaccion = limiteXTransaccion;
		this.modulos = modulos;
		this.nivelSeguridadAFP = nivelSeguridadAFP;
		this.nivelSeguridadCCre = nivelSeguridadCCre;
		this.nivelSeguridadISSS = nivelSeguridadISSS;
		this.nivelSeguridadPImp = nivelSeguridadPImp;
		this.nivelSeguridadPPag = nivelSeguridadPPag;
		this.nivelSeguridadTEx = nivelSeguridadTEx;
		this.nombre = nombre;
		this.nombreContacto = nombreContacto;
		this.nombreRepresentante = nombreRepresentante;
		this.numInstalacion = numInstalacion;
		this.permiteDebitos = permiteDebitos;
		this.permiteSMSToken = permiteSMSToken;
		this.politicaCobroTEx = politicaCobroTEx;
		this.proveedorInternet = proveedorInternet;
		this.solicitudPendiente = solicitudPendiente;
		this.telefono = telefono;
		this.telefonoSoporte = telefonoSoporte;
		this.tipoDoctoRepresentante = tipoDoctoRepresentante;
	}
	public BsClienteDto() {
	}
}
