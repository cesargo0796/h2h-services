package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHeaderConsultarServiciosPublicos {
    public String canal;
    public String idTransaccion;
    public String jornada;
    public String modoDeOperacion;
    public String nombreOperacion;
    public String perfil;
    public String total;
    public String usuario;
    public String versionServicio;

}
