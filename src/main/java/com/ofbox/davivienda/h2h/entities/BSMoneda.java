package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The persistent class for the BS_Moneda database table.
 * 
 */
@Entity
@NamedQuery(name="BSMoneda.findAll", query="SELECT b FROM BSMoneda b")
@Getter
@Setter
public class BSMoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Moneda")
	private Long moneda;

	@Column(name="Nombre")
	private String nombre;

	@Column(name="Simbolo")
	private String simbolo;

	//bi-directional many-to-one association to BS_Cuenta
	@OneToMany(mappedBy="bsMoneda")
	private List<BSCuenta> bsCuentas;
}