package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataConsultarPropiedadesColectorRequest {
    public String codColector;
    public String codIdioma;
    public String codPais;
    public String idSesion;
    public String valAccion;
    public String valBarra;
	@JsonProperty("valNPE")
    public String valNpe;
    public String valNumeroIdentificacion;
    public String valOrigen;
    public String valTipoIdentificacion;

}
