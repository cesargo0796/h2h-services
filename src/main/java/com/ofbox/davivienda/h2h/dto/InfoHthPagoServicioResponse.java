package com.ofbox.davivienda.h2h.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoHthPagoServicioResponse {
	
	private Integer codigo;
	private String descripcion;

}
