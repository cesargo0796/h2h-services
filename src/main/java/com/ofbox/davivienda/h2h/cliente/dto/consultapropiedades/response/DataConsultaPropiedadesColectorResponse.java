package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataConsultaPropiedadesColectorResponse {
	private String codColector;
	@JsonProperty("listaPropiedadesColector")
	private List<ListaPropiedadesColectorResponse> listaPropiedadesColector;
}
