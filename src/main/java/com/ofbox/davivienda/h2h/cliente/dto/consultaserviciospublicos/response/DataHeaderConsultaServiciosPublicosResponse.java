package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHeaderConsultaServiciosPublicosResponse {

	private String caracterAceptacion;
	private Integer codMsgRespuesta;
	private String idTransaccion;
	private String msgRespuesta;
	private String nombreOperacion;
	private Integer total;
	private String ultimoMensaje;
}
