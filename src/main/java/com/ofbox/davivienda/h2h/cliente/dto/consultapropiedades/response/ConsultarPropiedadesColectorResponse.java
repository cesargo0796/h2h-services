
package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultarPropiedadesColectorResponse {
	@JsonProperty("response")
	private ResponseConsultarPropiedadesColector response;
}