package com.ofbox.davivienda.h2h.controller.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.business.InvocadorJ2Entorno;
import com.ofbox.davivienda.h2h.constants.Constants;
import com.ofbox.davivienda.h2h.dto.InfoValidacionCuentaRequest;
import com.ofbox.davivienda.h2h.dto.InfoValidacionCuentaResponse;
import com.ofbox.davivienda.h2h.util.Utilities;

@RestController
@RequestMapping("Cuentas")
public class CuentasController {
	
	@PostMapping(path = "ValidarCuenta")
	public ResponseEntity<?> crearCodigoBanco(@RequestBody @Validated InfoValidacionCuentaRequest infoValidacionCuentaRequest) {		
		InfoValidacionCuentaResponse response = new InfoValidacionCuentaResponse();
		InvocadorJ2Entorno invocadorJ2Entorno = new InvocadorJ2Entorno();
		if(infoValidacionCuentaRequest == null)
		{	
			response.setCodigo(5);
			response.setDescripcion("Datos incompletos");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroCuenta())) {
			response.setCodigo(6);
			response.setDescripcion("Numero de cuenta es requerido");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);			
		}
		
		if(!Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroDocumento())) {
			if(Utilities.validarCampoVacio(infoValidacionCuentaRequest.getTipoDocumento())) {
				response.setCodigo(5);
				response.setDescripcion("Datos incompletos - tipo de documento no ingresado");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);					
			}	
			if(!Constants.DOCUMENT_TYPES.contains(infoValidacionCuentaRequest.getTipoDocumento())) {
				response.setCodigo(5);
				response.setDescripcion("Datos incorrectos - tipo de documento incorrecto");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);		
				
			}
		}
		
		//viene numCuenta
		Respuesta respuestaCuenta = invocadorJ2Entorno.invocarInformacionCuentaDeposito
		(infoValidacionCuentaRequest.getNumeroCuenta());
		

		if(respuestaCuenta == null) {
			response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setDescripcion("No ha sido posible obtener informacion de la cuenta de deposito");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		response.setCodigo(respuestaCuenta.getCodigo());
		response.setDescripcion(respuestaCuenta.getDescripcion());
		if(respuestaCuenta.getCodigo() != 0) {
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		
		if(!Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroDocumento())) {			
			//viene numDocumento
			Respuesta respuestaNiu = invocadorJ2Entorno.invocarInformacionNiu		
					(infoValidacionCuentaRequest.getNumeroDocumento(), infoValidacionCuentaRequest.getTipoDocumento());
			
			if(respuestaNiu == null) {
				response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
				response.setDescripcion("No ha sido posible obtener informacion del Niu");
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
			}

			response.setCodigo(respuestaNiu.getCodigo());
			response.setDescripcion(respuestaNiu.getDescripcion());
			
			if(respuestaNiu.getCodigo() == 0) {
				Respuesta respuestaCliente = invocadorJ2Entorno.invocarInformacionNiu		
						(infoValidacionCuentaRequest.getNumeroDocumento(), infoValidacionCuentaRequest.getTipoDocumento());		
				
				if(respuestaCliente == null) {
					response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
					response.setDescripcion("No ha sido posible obtener informacion del Niu por cliente");
					return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
				}
			}
			else {
				return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);				
			}
		}
		response.setCodigo(0);
		response.setDescripcion("Cuenta valida");		
		return new ResponseEntity<>(response, HttpStatus.OK);	

		/*
		 * Campo cuenta debe ser obligatorio
		 * Si viene numero de documento, que venga el tipo de documento y visceversa
		 * Que el tipo de documento sea segun el catalogo DUI , NIT, PASAPORTE, CARNET
		 * Si solo viene la cuenta, que solo invoque el servicio invocarInformacionCuentaDeposito
		 * Si viene documento con numero y tipo, se invocara el servicio obtenerNiu, si la respuesta es exitosa, se invocara el servicio invocarInformacionCliente
		 * 			
		 */
	}

}
