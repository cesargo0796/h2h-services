package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListServicio {
    public String codBarras;
	@JsonProperty("codNPE")
    public String codNpe;
    public String codTipoConvenio;
	@JsonProperty("listaPropiedadesAdicionales")
    public List<ListaPropiedadesAdicionales> listaPropiedadesAdicionales;

}
