package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHeaderPagoServiciosRequest {
    public String canal;
    public String idTransaccion;
    public String jornada;
    public String modoDeOperacion;
    public String nombreOperacion;
    public String perfil;
    public String total;
    public String usuario;
    public String versionServicio;

}
