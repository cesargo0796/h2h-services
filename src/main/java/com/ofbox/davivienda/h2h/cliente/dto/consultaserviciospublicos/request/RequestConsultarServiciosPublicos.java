package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestConsultarServiciosPublicos {
	@JsonProperty("data")
	private DataConsultarServiciosPublicos data;
	@JsonProperty("dataHeader")
	private DataHeaderConsultarServiciosPublicos dataHeader;

}
