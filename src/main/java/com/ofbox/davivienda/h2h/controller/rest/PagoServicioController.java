package com.ofbox.davivienda.h2h.controller.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request.ConsultarPropiedadesColectorRequest;
import com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request.DataConsultarPropiedadesColectorRequest;
import com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request.DataHeaderConsultarPropiedadesColectorRequest;
import com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request.RequestConsultarPropiedadesColector;
import com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response.ConsultarPropiedadesColectorResponse;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.ConsultarServiciosPublicosRequest;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.DataConsultarServiciosPublicos;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.DataHeaderConsultarServiciosPublicos;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.ListServicio;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.ListaPropiedadesAdicionales;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request.RequestConsultarServiciosPublicos;
import com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response.ConsultaServiciosPublicosResponse;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.DataHeaderPagoServiciosRequest;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.DataPagoServiciosRequest;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.Dato;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.PagoServiciosRequest;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.ProductoDebitar;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.PropiedadesColector;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request.RequestPagoServicios;
import com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.response.PagoServiciosPublicosResponse;
import com.ofbox.davivienda.h2h.dto.BsClienteDto;
import com.ofbox.davivienda.h2h.dto.HthPagoServicioDto;
import com.ofbox.davivienda.h2h.dto.InfoHthPagoServicioRequest;
import com.ofbox.davivienda.h2h.dto.InfoHthPagoServicioResponse;
import com.ofbox.davivienda.h2h.repositories.IBsClienteRepository;
import com.ofbox.davivienda.h2h.repositories.IHthPagoServicioRepository;
import com.ofbox.davivienda.h2h.util.Utilities;

@RestController
@RequestMapping("PagoServicio")
public class PagoServicioController {
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${web.regional.url}")
	private String url;	
	
	@Value("${web.regional.uri.consulta.propiedades}")
	private String uriConsultaPropiedades;
	
	@Value("${web.regional.uri.consulta.servicios.publicos}")
	private String uriConsultaServiciosPublicos;
	
	@Value("${web.regional.uri.consulta.propiedades}")
	private String uriPagoServiciosPublicos;
	
	@Autowired
	private IHthPagoServicioRepository hthPagoServicioRepository; 
	@Autowired
	private IBsClienteRepository bsClienteRepository;
	
	@PostMapping(path = "ConsultarPagoIndividual")
	public ResponseEntity<?> crearCodigoBanco(@RequestBody InfoHthPagoServicioRequest infoPagoServicioRequest) {
		ConsultarPropiedadesColectorResponse consultarPropiedadesColectorResponse;
		ConsultaServiciosPublicosResponse consultaServiciosPublicosResponse;
		PagoServiciosPublicosResponse pagoServiciosPublicosResponse;
		InfoHthPagoServicioResponse response = new InfoHthPagoServicioResponse();
		response.setCodigo(HttpStatus.OK.value());
		
		List<HthPagoServicioDto> listPagoServicios = hthPagoServicioRepository
				.findByNumCuenta(infoPagoServicioRequest.getNumeroCuenta(),infoPagoServicioRequest.getIdCliente());
		
		if(Utilities.validarListaVacia(listPagoServicios)) {
			response.setCodigo(HttpStatus.BAD_REQUEST.value());
			response.setDescripcion("Numero de cuenta o cliente no existe");
			return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
		}
		for(HthPagoServicioDto pagoServicioDto: listPagoServicios) {
			BsClienteDto clienteDto = bsClienteRepository.findByCliente(pagoServicioDto.getIdCliente());
			pagoServicioDto.setBsClienteDto(clienteDto);
		}
		
		try {		
			consultarPropiedadesColectorResponse =
					consultarPropiedadesColector(setConsultarPropiedadesColectorRequest(infoPagoServicioRequest)).getBody();	
			
		} catch (Exception e) {
			response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setDescripcion("Error en el servicio consulta propiedades colector. "+e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

		try {		
			consultaServiciosPublicosResponse =
			consultaServiciosPublicos(setConsultarServiciosPublicosRequest(infoPagoServicioRequest)).getBody();
			
		} catch (Exception e) {
			response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setDescripcion("Error en el servicio de consulta servicios publicos. "+e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		

		try {		
			pagoServiciosPublicosResponse =
			pagoServiciosPublicos(setPagoServiciosRequest(infoPagoServicioRequest)).getBody();
			
		} catch (Exception e) {
			response.setCodigo(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setDescripcion("Error en el servicio Pago servicios publicos. "+e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(listPagoServicios, HttpStatus.OK);
	}
	
	public ResponseEntity<ConsultarPropiedadesColectorResponse> 
	consultarPropiedadesColector(ConsultarPropiedadesColectorRequest request) throws Exception{
		String jsonString = Utilities.convertToJson(request);
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonString,Utilities.createHttpHeaders());
		ResponseEntity<ConsultarPropiedadesColectorResponse>response = 
				restTemplate
				.exchange(url+uriConsultaPropiedades, HttpMethod.POST,requestEntity,ConsultarPropiedadesColectorResponse.class);
		return new ResponseEntity<>(response.getBody(), HttpStatus.OK);	
	}

	public ResponseEntity<ConsultaServiciosPublicosResponse> consultaServiciosPublicos(ConsultarServiciosPublicosRequest request) throws Exception{
		String jsonString = Utilities.convertToJson(request);
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonString,Utilities.createHttpHeaders());
		ResponseEntity<ConsultaServiciosPublicosResponse>response = 
				restTemplate
				.exchange(url+uriConsultaServiciosPublicos, HttpMethod.POST,requestEntity,ConsultaServiciosPublicosResponse.class);
		return new ResponseEntity<>(response.getBody(), HttpStatus.OK);
		
	}
	
	public ResponseEntity<PagoServiciosPublicosResponse> pagoServiciosPublicos(PagoServiciosRequest request) throws Exception{
		String jsonString = Utilities.convertToJson(request);
		HttpEntity<String> requestEntity = new HttpEntity<>(jsonString,Utilities.createHttpHeaders());
		ResponseEntity<PagoServiciosPublicosResponse>response = 
				restTemplate
				.exchange(url+uriPagoServiciosPublicos, HttpMethod.POST,requestEntity,PagoServiciosPublicosResponse.class);
		return new ResponseEntity<>(response.getBody(), HttpStatus.OK);		
	}	
	
	

	
	public ConsultarPropiedadesColectorRequest setConsultarPropiedadesColectorRequest(InfoHthPagoServicioRequest infoPagoServicioRequest) {
		ConsultarPropiedadesColectorRequest requestConsultaPropiedades= new ConsultarPropiedadesColectorRequest();	
		RequestConsultarPropiedadesColector requestConsultarPropiedadesColector = new RequestConsultarPropiedadesColector();
		DataConsultarPropiedadesColectorRequest dataRequest = new DataConsultarPropiedadesColectorRequest();
		dataRequest.setCodColector("");
		dataRequest.setCodIdioma("ES");
		dataRequest.setCodPais("SV");
		dataRequest.setIdSesion("");
		dataRequest.setValAccion("");
		dataRequest.setValBarra("");
		dataRequest.setValNpe(infoPagoServicioRequest.getNpe());
		dataRequest.setValNumeroIdentificacion(String.valueOf(infoPagoServicioRequest.getIdCliente()));
		dataRequest.setValOrigen("");
		dataRequest.setValTipoIdentificacion(infoPagoServicioRequest.getNit());
		
		DataHeaderConsultarPropiedadesColectorRequest dataHeader = new DataHeaderConsultarPropiedadesColectorRequest();
		dataHeader.setCanal("");
		dataHeader.setIdTransaccion("");
		dataHeader.setJornada("");
		dataHeader.setModoDeOperacion("");
		dataHeader.setNombreOperacion("ConsultarPropiedadesColector");
		dataHeader.setPerfil("H2H");
		dataHeader.setTotal(0);
		dataHeader.setUsuario("");
		dataHeader.setVersionServicio("");
		
		requestConsultarPropiedadesColector.setData(dataRequest);
		requestConsultarPropiedadesColector.setDataHeader(dataHeader);
		
		
		requestConsultaPropiedades.setRequest(requestConsultarPropiedadesColector);
		return requestConsultaPropiedades;
	}
	
	public ConsultarServiciosPublicosRequest setConsultarServiciosPublicosRequest(InfoHthPagoServicioRequest infoPagoServicioRequest) {
		ConsultarServiciosPublicosRequest consultarServiciosPublicosRequest= new ConsultarServiciosPublicosRequest();	
		RequestConsultarServiciosPublicos requestConsultarServiciosPublicos = new RequestConsultarServiciosPublicos();
		DataConsultarServiciosPublicos dataRequest = new DataConsultarServiciosPublicos();
		dataRequest.setCodIdioma("ES");
		dataRequest.setCodPais("SV");
		dataRequest.setCodTipoIdentificacion(infoPagoServicioRequest.getNit());		
		dataRequest.setIdSesion("");
		dataRequest.setValOrigen("");
		dataRequest.setValNumeroIdentificacion(String.valueOf(infoPagoServicioRequest.getIdCliente()));
		
		List<ListServicio> listServicios = new ArrayList<>();
		ListServicio servicio= new ListServicio();
		servicio.setCodBarras("");
		servicio.setCodNpe(infoPagoServicioRequest.getNpe());
		servicio.setCodTipoConvenio("3");
		listServicios.add(servicio);
		
		List<ListaPropiedadesAdicionales> listaPropiedadesAdicionales = new ArrayList<>();

		servicio.setListaPropiedadesAdicionales(listaPropiedadesAdicionales);
		
		dataRequest.setListServicios(listServicios);
		
		DataHeaderConsultarServiciosPublicos dataHeader = new DataHeaderConsultarServiciosPublicos();
		dataHeader.setCanal("");
		dataHeader.setIdTransaccion("");
		dataHeader.setJornada("");
		dataHeader.setModoDeOperacion("");
		dataHeader.setNombreOperacion("ConsultaServiciosPublicos");
		dataHeader.setPerfil("H2H");
		dataHeader.setTotal("");
		dataHeader.setUsuario("");
		dataHeader.setVersionServicio("");
		
		requestConsultarServiciosPublicos.setData(dataRequest);
		requestConsultarServiciosPublicos.setDataHeader(dataHeader);
		
		
		consultarServiciosPublicosRequest.setRequest(requestConsultarServiciosPublicos);
		return consultarServiciosPublicosRequest;
	}
	
	public PagoServiciosRequest setPagoServiciosRequest(InfoHthPagoServicioRequest infoPagoServicioRequest) {
		PagoServiciosRequest pagoServiciosRequest= new PagoServiciosRequest();	
		RequestPagoServicios requestPagoServicios = new RequestPagoServicios();
		DataPagoServiciosRequest dataRequest = new DataPagoServiciosRequest();
		dataRequest.setCodColector("");
		dataRequest.setCodFactura("");
		dataRequest.setCodIdioma("ES");
		dataRequest.setCodMoneda("");
		dataRequest.setCodNpe(infoPagoServicioRequest.getNpe());
		dataRequest.setCodPais("SV");
		dataRequest.setCodTipoIdentificacion(infoPagoServicioRequest.getNit());		
		dataRequest.setIdSesion("");
		dataRequest.setValOrigen("");
		dataRequest.setValMonto("");
		dataRequest.setValNumeroIdentificacion(String.valueOf(infoPagoServicioRequest.getIdCliente()));
		
		List<Dato> datos = new ArrayList<>();
		Dato dato= new Dato();
		dato.setCodDato("");
		dato.setValDescripcionDato("");
		datos.add(dato);
		
		dataRequest.setDatos(datos);
		
		ProductoDebitar productoDebitar = new ProductoDebitar();	
		productoDebitar.setValFamiliaProducto("");
		productoDebitar.setValNumeroProducto("");
		productoDebitar.setValSubtipoProducto("");
		productoDebitar.setValTipoProducto("");
		
		dataRequest.setProductoDebitar(productoDebitar);
		
		List<PropiedadesColector> propiedadesColector = new ArrayList<>();
		PropiedadesColector propiedadColector=new PropiedadesColector();
		propiedadColector.setCodAtributo("");
		propiedadColector.setValAtributo("");
		propiedadesColector.add(propiedadColector);
		
		dataRequest.setPropiedadesColector(propiedadesColector);
		
		DataHeaderPagoServiciosRequest dataHeader = new DataHeaderPagoServiciosRequest();
		dataHeader.setCanal("");
		dataHeader.setIdTransaccion("");
		dataHeader.setJornada("");
		dataHeader.setModoDeOperacion("");
		dataHeader.setNombreOperacion("PagoServiciosPublicos");
		dataHeader.setPerfil("H2H");
		dataHeader.setTotal("");
		dataHeader.setUsuario("");
		dataHeader.setVersionServicio("");
		
		requestPagoServicios.setData(dataRequest);
		requestPagoServicios.setDataHeader(dataHeader);
		
		
		pagoServiciosRequest.setRequest(requestPagoServicios);
		return pagoServiciosRequest;
	}
}
