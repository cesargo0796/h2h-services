package com.ofbox.davivienda.h2h.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ofbox.davivienda.h2h.dto.BsClienteDto;
import com.ofbox.davivienda.h2h.entities.BsCliente;

@Repository
public interface IBsClienteRepository extends JpaRepository<BsCliente,Long> {
	
	@Query("SELECT new com.ofbox.davivienda.h2h.dto.BsClienteDto"
			+ "(b.cliente, b.apellidoContacto, b.apellidoRepresentante, b.banca,"
			+ "			b.bloqueado, b.cargaPersonalizable, b.cargo, b.ciudad, b.codigoTablaTransExt,"
			+ "			b.comisionTransExt, b.comisionTrnACH, b.direccion, b.doctoRepresentante, b.email,"
			+ "			b.estatus, b.fax, b.fechaCreacion, b.fechaEstatus, b.limiteCreditos,"
			+ "			b.limiteDebitos, b.limiteXArchivo, b.limiteXLote, b.limiteXTransaccion, b.modulos,"
			+ "			b.nivelSeguridadAFP, b.nivelSeguridadCCre, b.nivelSeguridadISSS, b.nivelSeguridadPImp,"
			+ "			b.nivelSeguridadPPag, b.nivelSeguridadTEx, b.nombre, b.nombreContacto,"
			+ "			b.nombreRepresentante, b.numInstalacion, b.permiteDebitos, b.permiteSMSToken,"
			+ "			b.politicaCobroTEx, b.proveedorInternet, b.solicitudPendiente, b.telefono,"
			+ "			b.telefonoSoporte, b.tipoDoctoRepresentante) "
			+ "FROM BsCliente b WHERE b.cliente = :cliente")
	BsClienteDto findByCliente(@Param("cliente") Long cliente);
}
