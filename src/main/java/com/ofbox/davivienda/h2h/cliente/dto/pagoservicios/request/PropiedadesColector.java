package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropiedadesColector {
	public String codAtributo;
	public String valAtributo;
}
