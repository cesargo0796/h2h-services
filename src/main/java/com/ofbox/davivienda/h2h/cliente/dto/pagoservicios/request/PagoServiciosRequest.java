package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagoServiciosRequest{     
	@JsonProperty("request")
	private RequestPagoServicios request; 
}