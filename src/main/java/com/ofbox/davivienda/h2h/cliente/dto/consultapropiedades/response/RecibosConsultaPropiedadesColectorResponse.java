package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecibosConsultaPropiedadesColectorResponse {
	private String codTipoIdentificacion;
	private String valNumeroIdentificacion;
	private String valMonto;
	private String codMoneda;
	private String valNumeroRecibo;
	private String valDescripcion;
	private String valEditable;
	private String valMontoMinimo;
	private String valMontoMaximo;
	@JsonProperty("listaPropiedadesAdicionales")
	private List<ListaPropiedadesAdicionalesResponse> listaPropiedadesAdicionales;
}