package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataPagoServiciosRequest {
    public String codBarra;
    public String codColector;
    public String codFactura;
    public String codIdioma;
    public String codMoneda;
	@JsonProperty("codNPE")
    public String codNpe;
    public String codPais;
    public String codTipoIdentificacion;
    public String idSesion;
    public String valMonto;
    public String valNumeroIdentificacion;
    public String valOrigen;
	@JsonProperty("datos")
    public List<Dato> datos;
	@JsonProperty("productoDebitar")
    public ProductoDebitar productoDebitar;
	@JsonProperty("propiedadesColector")
    public List<PropiedadesColector> propiedadesColector;

}
