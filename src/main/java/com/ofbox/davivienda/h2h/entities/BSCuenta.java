package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the BS_Cuenta database table.
 * 
 */
@Entity
@NamedQuery(name="BSCuenta.findAll", query="SELECT b FROM BSCuenta b")
@Getter
@Setter
public class BSCuenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Cuenta")
	private String cuenta;

	@Column(name="AliasCuenta")
	private String aliasCuenta;

	@Column(name="Cliente")
	private Long cliente;

	@Column(name="CuentaMadre")
	private String cuentaMadre;

	@Column(name="CuotaOrdinaria")
	private BigDecimal cuotaOrdinaria;

	@Column(name="EsCrediExpress")
	private Long esCrediExpress;

	@Column(name="EsInteligente")
	private Long esInteligente;

	@Column(name="EsquemaCtaInteligente")
	private Long esquemaCtaInteligente;

	@Column(name="Estatus")
	private Long estatus;

	@Column(name="EstatusHost")
	private Long estatusHost;

	@Column(name="FechaEstatus")
	private Date fechaEstatus;

	@Column(name="FechaOtorgado")
	private Date fechaOtorgado;

	@Column(name="FechaUltimaActualizacion")
	private Date fechaUltimaActualizacion;

	@Column(name="FechaVencimiento")
	private Date fechaVencimiento;

	@Column(name="Limite")
	private BigDecimal limite;

	@Column(name="NIU")
	private Long niu;

	@Column(name="Nombre")
	private String nombre;

	//bi-directional many-to-one association to BS_Moneda
	@ManyToOne
	@JoinColumn(name="Moneda")
	private BSMoneda bsMoneda;

	//bi-directional many-to-one association to BS_TipoCuenta
	@ManyToOne
	@JoinColumn(name="TipoCuenta")
	private BSTipoCuenta bsTipoCuenta;

	//bi-directional many-to-one association to BS_PagoServicio
	@OneToMany(mappedBy="bsCuenta")
	private List<BSPagoServicio> bsPagoServicios;
}