package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The persistent class for the BS_TipoCuenta database table.
 * 
 */
@Entity
@NamedQuery(name="BSTipoCuenta.findAll", query="SELECT b FROM BSTipoCuenta b")
@Getter
@Setter
public class BSTipoCuenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TipoCuenta")
	private Long tipoCuenta;

	private Long idAS400;

	private Long idTipoOperacion;

	@Column(name="Nombre")
	private String nombre;

	@Column(name="NombreCorto")
	private Long nombreCorto;

	//bi-directional many-to-one association to BS_Cuenta
	@OneToMany(mappedBy="bsTipoCuenta")
	private List<BSCuenta> bsCuentas;
}