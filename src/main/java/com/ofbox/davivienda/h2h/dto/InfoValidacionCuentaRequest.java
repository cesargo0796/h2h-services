package com.ofbox.davivienda.h2h.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoValidacionCuentaRequest {
	@NotNull
	@NotEmpty
	private String numeroCuenta;
	private String tipoCuenta;
	private String tipoDocumento;
	private String numeroDocumento;
}
