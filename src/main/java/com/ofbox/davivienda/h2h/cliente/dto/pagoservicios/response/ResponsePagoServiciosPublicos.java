package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponsePagoServiciosPublicos {
	@JsonProperty("data")
	private DataPagoServiciosPublicosResponse data;
	@JsonProperty("dataHeader")
	private DataHeaderPagoServiciosPublicosResponse dataHeader;

}
