package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecibosConsultaServiciosPublicosResponse {
	private String codMoneda;
	private String codTipoIdentificacion;
	private String valDescripcion;
	private String valEditable;
	private String valMonto;
	private String valMontoMinimo;
	private String valMontoMaximo;
	private String valNumeroIdentificacion;
	private String valNumeroRecibo;
	private List<ListaPropiedadesAdicionales> listaPropiedadesAdicionales;

}
