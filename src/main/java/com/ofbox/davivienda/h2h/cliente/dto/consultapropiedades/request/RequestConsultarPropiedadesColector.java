package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestConsultarPropiedadesColector {
	@JsonProperty("data")
	private DataConsultarPropiedadesColectorRequest data;
	@JsonProperty("dataHeader")
	private DataHeaderConsultarPropiedadesColectorRequest dataHeader;
}
