package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the BS_Cliente database table.
 * 
 */
@Entity
@Table(name="BS_Cliente")
@NamedQuery(name="BsCliente.findAll", query="SELECT b FROM BsCliente b")
@Getter
@Setter
public class BsCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Cliente")
	private Long cliente;

	@Column(name="ApellidoContacto")
	private String apellidoContacto;

	private String apellidoRepresentante;

	@Column(name="Banca")
	private Long banca;

	@Column(name="Bloqueado")
	private Boolean bloqueado;

	@Column(name="CargaPersonalizable")
	private Boolean cargaPersonalizable;

	@Column(name="Cargo")
	private String cargo;

	@Column(name="Ciudad")
	private String ciudad;

	@Column(name="CodigoTablaTransExt")
	private Long codigoTablaTransExt;

	@Column(name="ComisionTransExt")
	private Double comisionTransExt;

	@Column(name="ComisionTrnACH")
	private Double comisionTrnACH;

	@Column(name="Direccion")
	private String direccion;

	@Column(name="DoctoRepresentante")
	private String doctoRepresentante;

	@Column(name="Email")
	private String email;

	@Column(name="Estatus")
	private Long estatus;

	@Column(name="Fax")
	private String fax;

	@Column(name="FechaCreacion")
	private Date fechaCreacion;

	@Column(name="FechaEstatus")
	private Date fechaEstatus;

	@Column(name="LimiteCreditos")
	private Double limiteCreditos;

	@Column(name="LimiteDebitos")
	private Double limiteDebitos;

	@Column(name="LimiteXArchivo")
	private Double limiteXArchivo;

	@Column(name="LimiteXLote")
	private Double limiteXLote;

	@Column(name="LimiteXTransaccion")
	private Double limiteXTransaccion;

	@Column(name="Modulos")
	private Long modulos;

	@Column(name="NivelSeguridadAFP")
	private Long nivelSeguridadAFP;

	@Column(name="NivelSeguridadCCre")
	private Long nivelSeguridadCCre;

	@Column(name="NivelSeguridadISSS")
	private Long nivelSeguridadISSS;

	@Column(name="NivelSeguridadPImp")
	private Long nivelSeguridadPImp;

	@Column(name="NivelSeguridadPPag")
	private Long nivelSeguridadPPag;

	@Column(name="NivelSeguridadTEx")
	private Long nivelSeguridadTEx;

	@Column(name="Nombre")
	private String nombre;

	@Column(name="NombreContacto")
	private String nombreContacto;

	private String nombreRepresentante;

	@Column(name="NumInstalacion")
	private Long numInstalacion;

	@Column(name="PermiteDebitos")
	private Boolean permiteDebitos;

	@Column(name="PermiteSMSToken")
	private Boolean permiteSMSToken;

	@Column(name="PoliticaCobroTEx")
	private Long politicaCobroTEx;

	private String proveedorInternet;

	@Column(name="SolicitudPendiente")
	private Long solicitudPendiente;

	@Column(name="Telefono")
	private String telefono;

	@Column(name="TelefonoSoporte")
	private String telefonoSoporte;

	@Column(name="TipoDoctoRepresentante")
	private Long tipoDoctoRepresentante;

	@OneToMany(mappedBy="bsCliente")
	private List<HthPagoServicio> hthPagoServicios;
}