package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter 
public class ResponseConsultaServiciosPublicos {
	@JsonProperty("data")
	private DataConsultaServiciosPublicosResponse data;
	@JsonProperty("dataHeader")
	private DataHeaderConsultaServiciosPublicosResponse dataHeader;

}
