package com.ofbox.davivienda.h2h.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the HTH_PAGO_SERVICIOS database table.
 * 
 */
@Getter
@Setter
public class HthPagoServicioDto{

	private Long idPagoServicio;
	private String estado;
	private String numCuenta;
	
	@JsonIgnore
	private Long idCliente;
	private BsClienteDto bsClienteDto;

	public HthPagoServicioDto(Long idPagoServicio, String estado, String numCuenta, Long idCliente) {
		this.idPagoServicio = idPagoServicio;
		this.estado = estado;
		this.numCuenta = numCuenta;
		this.idCliente = idCliente;
	}

	public HthPagoServicioDto() {
	}	
}