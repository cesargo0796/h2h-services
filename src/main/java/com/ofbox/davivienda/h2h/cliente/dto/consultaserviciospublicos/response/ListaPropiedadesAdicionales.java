package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaPropiedadesAdicionales {

	private String idPropiedad;
	private String valDescripcion;
	private String valOrden;

}
