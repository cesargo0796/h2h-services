package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaPropiedadesColectorResponse {

	private String codIdAtributo;
	private String valAtributoPantalla;
	private String valEsObligatorio;
	private String valLeyendaDespliegue;
	private String valPedirPantalla; 
	private String valSecDespliegue;
	private String valTipoDatoPropiedadColector;
	@JsonProperty("listaValoresPosibleAtributo")
	private List<ListaValoresPosibleAtributoResponse> listaValoresPosibleAtributo;
}