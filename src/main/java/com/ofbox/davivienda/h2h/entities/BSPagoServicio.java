package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;
import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the BS_PagoServicio database table.
 * 
 */
@Entity
@NamedQuery(name="BSPagoServicio.findAll", query="SELECT b FROM BSPagoServicio b")
@Getter
@Setter
public class BSPagoServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="PagoServicio")
	private Long pagoServicio;

	@Column(name="Autorizacion")
	private String autorizacion;

	@Column(name="Cliente")
	private Long cliente;

	@Column(name="ComentarioRechazo")
	private String comentarioRechazo;

	@Lob
	@Column(name="ConsultaXml")
	private String consultaXml;

	@Column(name="Dato1")
	private String dato1;

	@Column(name="Dato2")
	private String dato2;

	@Column(name="Dato3")
	private String dato3;

	@Column(name="Dato4")
	private String dato4;

	@Column(name="Dato5")
	private String dato5;

	@Column(name="Dato6")
	private String dato6;

	@Lob
	@Column(name="DetalleXml")
	private String detalleXml;

	@Column(name="EsPagoDirecto")
	private Boolean esPagoDirecto;

	@Column(name="Estatus")
	private Long estatus;

	@Column(name="FechaCreacion")
	private Timestamp fechaCreacion;

	@Column(name="FechaDocumento")
	private Timestamp fechaDocumento;

	@Column(name="FechaEstatus")
	private Timestamp fechaEstatus;

	@Column(name="Instalacion")
	private Long instalacion;

	@Column(name="Lote")
	private Long lote;

	@Column(name="Moneda")
	private Long moneda;

	@Column(name="Monto")
	private BigDecimal monto;

	@Column(name="MontoImpuesto")
	private BigDecimal montoImpuesto;

	@Column(name="NombreDato1")
	private String nombreDato1;

	@Column(name="NombreDato2")
	private String nombreDato2;

	@Column(name="NombreDato3")
	private String nombreDato3;

	@Column(name="NombreDato4")
	private String nombreDato4;

	@Column(name="NombreDato5")
	private String nombreDato5;

	@Column(name="NombreDato6")
	private String nombreDato6;

	@Column(name="Proveedor")
	private Long proveedor;

	@Column(name="Token")
	private Long token;

	@Column(name="Usuario")
	private String usuario;

	@Lob
	@Column(name="XmlRespuestaConsulta")
	private String xmlRespuestaConsulta;

	//bi-directional many-to-one association to BS_Cuenta
	@ManyToOne
	@JoinColumn(name="CuentaDebito")
	private BSCuenta bsCuenta;
}