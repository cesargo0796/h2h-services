package com.ofbox.davivienda.h2h.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


/**
 * The persistent class for the HTH_PAGO_SERVICIOS database table.
 * 
 */
@Entity
@Table(name="HTH_PAGO_SERVICIOS")
@NamedQuery(name="HthPagoServicio.findAll", query="SELECT h FROM HthPagoServicio h")
@Getter
@Setter
public class HthPagoServicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IdPagoServicio")
	private Long idPagoServicio;

	@Column(name="Estado")
	private String estado;

	@Column(name="NumCuenta")
	private String numCuenta;

	@ManyToOne
	@JoinColumn(name="IdCliente")
	private BsCliente bsCliente;
}