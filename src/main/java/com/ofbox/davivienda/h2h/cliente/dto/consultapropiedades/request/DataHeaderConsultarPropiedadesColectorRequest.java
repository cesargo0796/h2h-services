package com.ofbox.davivienda.h2h.cliente.dto.consultapropiedades.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataHeaderConsultarPropiedadesColectorRequest {
	private String canal;
	private String idTransaccion;
	private String jornada;
	private String modoDeOperacion;
	private String nombreOperacion;
	private String perfil;
	private Integer total;
	private String usuario;
	private String versionServicio;

}
