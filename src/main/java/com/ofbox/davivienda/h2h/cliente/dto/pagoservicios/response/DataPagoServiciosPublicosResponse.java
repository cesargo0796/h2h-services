package com.ofbox.davivienda.h2h.cliente.dto.pagoservicios.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataPagoServiciosPublicosResponse {
	private String codMonedaCostoTransaccion;
	private String codMonedaTotalPago;
	private String valComision;
	private String valFechaHoraTransaccion;
	private String valNumeroComprobante;
	private String valTotalPago;

}
