package com.ofbox.davivienda.h2h.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InfoHthPagoServicioRequest {
	
	private String npe;
	private String numeroCuenta;
	private String nit;
	private Long idCliente;
}
