package com.ofbox.davivienda.h2h.cliente.dto.consultaserviciospublicos.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListaPropiedadesAdicionales {
    public String idPropiedad;
    public String valDescripcion;

}
